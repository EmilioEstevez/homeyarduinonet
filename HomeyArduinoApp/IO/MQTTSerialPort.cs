using System;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using System.Text;
using System.Threading.Tasks;
using HomeyArduinoTestApp.Configuration.Sections;
using HomeyArduinoTestApp.Interfaces;
using MQTTnet;
using MQTTnet.Client;

namespace HomeyArduinoTestApp.IO
{
    public class MQTTSerialPort<TOutput>: IObservableSerialPort<TOutput>
    {
        private readonly MQTTPortSection _section;
        private readonly IObservableDataParser<string, TOutput> _parser;
        private IMqttClient _client;
        private readonly Subject<TOutput> _dataReceived = new Subject<TOutput>();

        public MQTTSerialPort(MQTTPortSection section, IObservableDataParser<string, TOutput> parser)
        {
            _section = section;
            _parser = parser;

            Connect();
        }

        public IObservable<TOutput> DataReceived => _dataReceived.AsObservable();

        public async Task Connect()
        {
            var options = new MqttClientOptionsBuilder()
                .WithTcpServer(_section.Server, _section.Port) // Port is optional
                .Build();
            
            var factory = new MqttFactory();
            _client = factory.CreateMqttClient();
            
            _client.ApplicationMessageReceived += ClientOnApplicationMessageReceived;
            
            await _client.ConnectAsync(options);
            await _client.SubscribeAsync(_section.ReceiveTopic);
        }

        private void ClientOnApplicationMessageReceived(object sender, MqttApplicationMessageReceivedEventArgs e)
        {
            var data = Encoding.UTF8.GetString(e.ApplicationMessage.Payload);
            _dataReceived.OnNext(_parser.Parse(data));
        }
    }
}