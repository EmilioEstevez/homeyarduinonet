using System;
using System.IO.Ports;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using HomeyArduinoTestApp.Configuration;
using HomeyArduinoTestApp.Configuration.Sections;
using HomeyArduinoTestApp.Interfaces;

namespace HomeyArduinoTestApp.IO
{
    public class ObservableSerialPort<TOutput>: IDisposable
    {
        private readonly IObservableDataParser<string, TOutput> _parser;
        private readonly SerialPort _serialPort;
        private readonly Subject<TOutput> _dataReceived = new Subject<TOutput>();
        
        public ObservableSerialPort(SerialPortSection section, IObservableDataParser<string, TOutput> parser)
        {
            _parser = parser;
            _serialPort = new SerialPort(section.PortName, section.BaudRate, section.Parity, section.DataBits);
            
            _serialPort.DataReceived += SerialPortOnDataReceived;
            
            _serialPort.Open();
        }

        private void SerialPortOnDataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            while (_serialPort.BytesToRead > 0)
            {
                var line = _serialPort.ReadLine();
                _dataReceived.OnNext(_parser.Parse(line));
            }
        }

        private IObservable<TOutput> DataReceived => _dataReceived.AsObservable();

        public void Dispose()
        {
            _serialPort?.Dispose();
            _dataReceived?.Dispose();
        }
    }
}