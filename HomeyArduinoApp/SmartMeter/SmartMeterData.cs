namespace HomeyArduinoTestApp.SmartMeter
{
    public class SmartMeterData
    {
        public decimal LowFareMeterPosition { get; set; }

        public decimal HighFareMeterPosition { get; set; }

        public decimal LowFareProductionMeterPosition { get; set; }

        public decimal HighFareProductionMeterPosition { get; set; }

        public Fare CurrentFare { get; set; }

        public decimal CurrentPowerConsumption { get; set; }

        public decimal CurrentPowerProduction { get; set; }

        public decimal MaximumEnergyFlow { get; set; }

        public bool Enabled { get; set; }
        
        public decimal Voltage { get; set; }
        public decimal GasMeterPosition { get; set; }
        
    }
}