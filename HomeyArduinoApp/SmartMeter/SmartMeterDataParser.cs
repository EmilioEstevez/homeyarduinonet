using System.Text.RegularExpressions;
using HomeyArduinoTestApp.Interfaces;

namespace HomeyArduinoTestApp.SmartMeter
{
    public class SmartMeterDataParser: IObservableDataParser<string, SmartMeterData>
    {
        

        
        private const string LOW_FARE_POWER_CONSUMPTION_REGEX = @"1-0:1.8.1\((.*?)\*kWh\)";
        private const string HIGH_FARE_POWER_CONSUMPTION_REGEX = @"1-0:1.8.2\((.*?)\*kWh\)";
        private const string VOLTAGE_REGEX = @"1-0:32.7.0\((.*?)\*V\)"; //1-0:32.7.0(228.9*V)
        private const string GAS_REGEX = @"0-1:24.2.1\(.*?\)\((.*?)\*m3\)"; //0-1:24.2.1(190319203503W)(00729.337*m3)
        private const string LOW_FARE_POWER_PRODUCTION_REGEX = @"1-0:2.8.1\((.*?)\)";
        private const string HIGH_FARE_POWER_PRODUCTION_REGEX = @"1-0:2.8.2\((.*?)\)";
        private const string POWER_FARE_REGEX = @"0-0:96.14.0\((.*?)\)";
        private const string CURRENT_POWER_CONSUMPTION_REGEX = @"1-0:1.7.0\((.*?)\*kW\)";
        private const string CURRENT_POWER_PRODUCTION_REGEX = @"1-0:2.7.0\((.*?)\)";
//        private const string MAXMIMUM_POWER_FLOW_REGEX = @"0-0:17.0.0\((.*?)\)";
        private const string SWITCH_POSITION_REGEX = @"0-0:96.3.10\((.*?)\)";
        
        public SmartMeterData Parse(string input)
        {
            var result = new SmartMeterData();
            result.LowFareMeterPosition = decimal.Parse(ParseValue(input, HIGH_FARE_POWER_CONSUMPTION_REGEX));
            result.HighFareMeterPosition = decimal.Parse(ParseValue(input, LOW_FARE_POWER_CONSUMPTION_REGEX));
            result.CurrentFare = int.Parse(ParseValue(input, POWER_FARE_REGEX)) == 1 ? Fare.High : Fare.Low;
            result.CurrentPowerConsumption = decimal.Parse(ParseValue(input, CURRENT_POWER_CONSUMPTION_REGEX));
            result.Voltage = decimal.Parse(ParseValue(input, VOLTAGE_REGEX));
            result.GasMeterPosition = decimal.Parse(ParseValue(input, GAS_REGEX));
            return result;
        }

        private string ParseValue(string data, string pattern)
        {
            var match = Regex.Match(data, pattern);
            return match.Groups[1].Value;
        }
    }
}