﻿using System;
using System.Collections.Generic;
using System.Reactive.Linq;
using System.Threading;
using System.Timers;
using Autofac;
using HomeyArduinoNet;
using HomeyArduinoNet.Configuration;
using HomeyArduinoNet.Dto;
using HomeyArduinoTestApp.Capabilities;
using HomeyArduinoTestApp.Configuration;
using HomeyArduinoTestApp.Configuration.Sections;
using HomeyArduinoTestApp.IO;
using HomeyArduinoTestApp.SmartMeter;
using Microsoft.Extensions.Configuration;

namespace HomeyArduinoTestApp
{
    public class Program
    {
        private static readonly AutoResetEvent AutoResetEvent = new AutoResetEvent(false);
        
        static void Main(string[] args)
        {
            var program = new Program();
            program.Start();
            program.Run();
            
        }

        public void Start()
        {
            const string HIGH_POWER_CONSUMPTION = "highPowerConsumption";
            const string LOW_POWER_CONSUMPTION = "lowPowerConsumption";
            
            var configurationBuilder = new ConfigurationBuilder();
            configurationBuilder.AddJsonFile("appsettings.json");
            var config = configurationBuilder.Build();

            var configReader = new ConfigReader(config);
            var builder = new ContainerBuilder();

            var mqttSection = configReader.ReadSection<MQTTPortSection>();
            builder.RegisterInstance(mqttSection);
            builder.RegisterType<MQTTSerialPort<SmartMeterData>>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<SmartMeterDataParser>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<CurrentElectricityUsageCapability>().SingleInstance().AsSelf();
            builder.RegisterType<HighFarePowerCapability>().SingleInstance().AsSelf();
            builder.RegisterType<LowFarePowerCapability>().SingleInstance().AsSelf();
            builder.RegisterType<TotalPowerCapability>().SingleInstance().AsSelf();
            builder.RegisterType<VoltageCapability>().SingleInstance().AsSelf();
            builder.RegisterType<GasCapability>().SingleInstance().AsSelf();

            var container = builder.Build();
            
            var deviceConfiguration = new HomeyDevice
            {
                Id = "example",
                Type = "homeyduinoTest",
                Class = "socket",
                Master = new MasterConfiguration
                {
                    Port = 36473,
                    Host = "192.168.1.158"
                },
                Version = "0.1.0",
                Api = new List<HomeyApi>
                {
//                    new HomeyApiCapability("onoff", new CapabilityStatus(HomeyFieldType.Boolean, false)),
                    new HomeyApiCapability("measure_power", container.Resolve<CurrentElectricityUsageCapability>()),
                    new HomeyApiCapability("meter_power", container.Resolve<TotalPowerCapability>()),
//                    new HomeyApiCapability("meter_power.high", container.Resolve<HighFarePowerCapability>()),
//                    new HomeyApiCapability("meter_power.low", container.Resolve<LowFarePowerCapability>()),
                    new HomeyApiCapability("measure_voltage", container.Resolve<VoltageCapability>()),
                    new HomeyApiCapability("meter_gas", container.Resolve<GasCapability>())
                }
            };
//            var x = Observable.Interval(TimeSpan.FromSeconds(3)).Subscribe(_ => { test.Value = (int)test.Value + 1; });
            var homeyDevice = new HomeyDeviceHost(deviceConfiguration);
        }

        public void Run()
        {
            Console.CancelKeyPress += (sender, eventArgs) => AutoResetEvent.Set();
            AutoResetEvent.WaitOne();
        }
    }
}