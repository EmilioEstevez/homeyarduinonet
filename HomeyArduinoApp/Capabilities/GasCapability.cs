using HomeyArduinoNet.Dto;
using HomeyArduinoTestApp.Interfaces;
using HomeyArduinoTestApp.SmartMeter;

namespace HomeyArduinoTestApp.Capabilities
{
    public class GasCapability : SmartMeterCapabilityBase
    {
        public GasCapability(IObservableSerialPort<SmartMeterData> serialPort)
            : base(HomeyFieldType.Number, serialPort, 0)
        { }

        protected override void OnDataReceived(SmartMeterData data)
        {
            Value = data.GasMeterPosition;
        }
    }
}