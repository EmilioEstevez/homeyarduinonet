using HomeyArduinoNet.Dto;
using HomeyArduinoTestApp.Interfaces;
using HomeyArduinoTestApp.SmartMeter;

namespace HomeyArduinoTestApp.Capabilities
{
    public class HighFarePowerCapability: SmartMeterCapabilityBase
    {
        public HighFarePowerCapability(IObservableSerialPort<SmartMeterData> serialPort)
            : base(HomeyFieldType.Number, serialPort, 0)
        {
        }

        protected override void OnDataReceived(SmartMeterData data)
        {
            Value = data.HighFareMeterPosition;
        }
    }
    
    public class TotalPowerCapability: SmartMeterCapabilityBase
    {
        public TotalPowerCapability(IObservableSerialPort<SmartMeterData> serialPort)
            : base(HomeyFieldType.Number, serialPort, 0)
        {
        }

        protected override void OnDataReceived(SmartMeterData data)
        {
            Value = data.HighFareMeterPosition + data.LowFareMeterPosition;
        }
    }
}