using HomeyArduinoNet.Dto;
using HomeyArduinoTestApp.Interfaces;
using HomeyArduinoTestApp.SmartMeter;

namespace HomeyArduinoTestApp.Capabilities
{
    public class LowFarePowerCapability: SmartMeterCapabilityBase
    {
        public LowFarePowerCapability(IObservableSerialPort<SmartMeterData> serialPort)
            : base(HomeyFieldType.Number, serialPort, 0)
        {
        }

        protected override void OnDataReceived(SmartMeterData data)
        {
            Value = data.LowFareMeterPosition; 
        }
    }
}