using HomeyArduinoNet.Dto;
using HomeyArduinoTestApp.Interfaces;
using HomeyArduinoTestApp.SmartMeter;

namespace HomeyArduinoTestApp.Capabilities
{
    public class CurrentElectricityUsageCapability : SmartMeterCapabilityBase
    {
        public CurrentElectricityUsageCapability(IObservableSerialPort<SmartMeterData> serialPort)
            : base(HomeyFieldType.Number, serialPort, 0)
        { }

        protected override void OnDataReceived(SmartMeterData data)
        {
            Value = data.CurrentPowerConsumption * 1000;
        }
    }
}