using System;
using System.Threading.Tasks;
using HomeyArduinoNet.Configuration;
using HomeyArduinoNet.Dto;
using HomeyArduinoTestApp.Interfaces;
using HomeyArduinoTestApp.SmartMeter;

namespace HomeyArduinoTestApp.Capabilities
{
    public abstract class SmartMeterCapabilityBase : CapabilityStatus, IDisposable
    {
        private readonly IDisposable _serialPortSubscription;

        protected SmartMeterCapabilityBase(HomeyFieldType type, IObservableSerialPort<SmartMeterData> serialPort, object initialValue) : base(type, initialValue)
        {
            _serialPortSubscription = serialPort.DataReceived.Subscribe(data => OnDataReceived(data));
        }

        protected abstract void OnDataReceived(SmartMeterData data);

        public void Dispose()
        {
            _serialPortSubscription.Dispose();
        }
    }
}