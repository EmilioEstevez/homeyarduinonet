using HomeyArduinoNet.Configuration;
using HomeyArduinoNet.Dto;
using HomeyArduinoTestApp.Interfaces;
using HomeyArduinoTestApp.SmartMeter;

namespace HomeyArduinoTestApp.Capabilities
{
    public class VoltageCapability: SmartMeterCapabilityBase
    {
        public VoltageCapability(IObservableSerialPort<SmartMeterData> serialPort)
            : base(HomeyFieldType.Number, serialPort, 0)
        {
        }

        protected override void OnDataReceived(SmartMeterData data)
        {
            Value = data.Voltage;
        }
    }
}