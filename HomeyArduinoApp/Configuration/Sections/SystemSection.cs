namespace HomeyArduinoTestApp.Configuration.Sections
{
    public class SystemSection : ConfigSectionBase
    {
        public string Id { get; set; }
        public string Type { get; set; }
        public string @Class { get; set; }
        
    }
}