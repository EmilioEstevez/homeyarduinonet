using System;
using HomeyArduinoTestApp.Interfaces;

namespace HomeyArduinoTestApp.Configuration.Sections
{
    public abstract class ConfigSectionBase: IConfigSection
    {
        protected ConfigSectionBase()
        {
            SectionName = GetType().Name.Replace("Section", String.Empty);
        }
        
        public string SectionName { get; set; }
    }
}