using System.IO.Ports;

namespace HomeyArduinoTestApp.Configuration.Sections
{
    public class SerialPortSection: ConfigSectionBase
    {
        public string PortName { get; set; }
        public int BaudRate { get; set; }
        public Parity Parity { get; set; }
        public int DataBits { get; set; }
    }
}