namespace HomeyArduinoTestApp.Configuration.Sections
{
    public class MQTTPortSection : ConfigSectionBase
    {
        public string ReceiveTopic { get; set; }
        
        public string Server { get; set; }
        
        public int Port { get; set; }
    }
}