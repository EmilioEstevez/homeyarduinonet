using HomeyArduinoTestApp.Interfaces;
using Microsoft.Extensions.Configuration;

namespace HomeyArduinoTestApp.Configuration
{
    public class ConfigReader: IConfigReader
    {
        private const string CONFIGURATION_SECTION = "Configuration";
        private readonly IConfigurationRoot _configurationRoot;

        public ConfigReader(IConfigurationRoot configurationRoot)
        {
            _configurationRoot = configurationRoot;
        }

        public TSection ReadSection<TSection>(TSection section) where TSection : IConfigSection
        {
            _configurationRoot.GetSection(CONFIGURATION_SECTION).Bind(section.SectionName, section);
            return section;
        }

        public TSection ReadSection<TSection>() where TSection : IConfigSection, new()
            => ReadSection(new TSection());
    }
}