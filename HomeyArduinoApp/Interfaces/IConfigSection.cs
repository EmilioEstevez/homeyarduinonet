namespace HomeyArduinoTestApp.Interfaces
{
    public interface IConfigSection
    {
        string SectionName { get; }
    }
}