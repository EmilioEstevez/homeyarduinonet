using System;

namespace HomeyArduinoTestApp.Interfaces
{
    public interface IObservableSerialPort<out TOutput>
    {
        IObservable<TOutput> DataReceived { get; }
    }
}