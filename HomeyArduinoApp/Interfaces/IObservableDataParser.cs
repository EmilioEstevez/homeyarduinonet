namespace HomeyArduinoTestApp.Interfaces
{
    public interface IObservableDataParser<TInput, TOutput>
    {
        TOutput Parse(TInput input);
    }
}