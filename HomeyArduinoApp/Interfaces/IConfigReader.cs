namespace HomeyArduinoTestApp.Interfaces
{
    public interface IConfigReader
    {
        TSection ReadSection<TSection>(TSection section) where TSection : IConfigSection;
        TSection ReadSection<TSection>() where TSection : IConfigSection, new();
    }
}