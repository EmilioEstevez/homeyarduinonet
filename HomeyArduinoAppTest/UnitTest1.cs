using HomeyArduinoTestApp.SmartMeter;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace HomeyArduinoAppTest
{
    [TestClass]
    public class UnitTest1
    {
        private const string DATA = @"/ISK5\2M550E-1012
1-3:0.2.8(50)
0-0:1.0.0(190319203735W)
0-0:96.1.1(4530303433303036393330383433333137)
1-0:1.8.1(001497.785*kWh)
1-0:1.8.2(001417.359*kWh)
1-0:2.8.1(000000.000*kWh)
1-0:2.8.2(000000.000*kWh)
0-0:96.14.0(0002)
1-0:1.7.0(00.322*kW)
1-0:2.7.0(00.000*kW)
0-0:96.7.21(00013)
0-0:96.7.9(00003)
1-0:99.97.0(1)(0-0:96.7.19)(170705190146S)(0000000281*s)
1-0:32.32.0(00006)
1-0:32.36.0(00001)
0-0:96.13.0()
1-0:32.7.0(228.9*V)
1-0:31.7.0(001*A)
1-0:21.7.0(00.321*kW)
1-0:22.7.0(00.000*kW)
0-1:24.1.0(003)
0-1:96.1.0(4730303339303031373335333939303137)
0-1:24.2.1(190319203503W)(00729.337*m3)";

        private SmartMeterDataParser _parser = new SmartMeterDataParser();
        
        [TestMethod]
        public void Parse_ShouldParseLowFareMeterPosition()
        {
            // act
            var result = _parser.Parse(DATA);
            
            // assert
            Assert.AreEqual(1497.785m, result.LowFareMeterPosition);
        }
        
        [TestMethod]
        public void Parse_ShouldParseHighFareMeterPosition()
        {
            // act
            var result = _parser.Parse(DATA);
            
            // assert
            Assert.AreEqual(1417.359m, result.HighFareMeterPosition);
        }
        
        [TestMethod]
        public void Parse_ShouldParseCurrentPowerConsumption()
        {
            // act
            var result = _parser.Parse(DATA);
            
            // assert
            Assert.AreEqual(0.322m, result.CurrentPowerConsumption);
        }
        
        [TestMethod]
        public void Parse_ShouldParseVoltage()
        {
            // act
            var result = _parser.Parse(DATA);
            
            // assert
            Assert.AreEqual(228.9m, result.Voltage);
        }
        
        [TestMethod]
        public void Parse_ShouldParseGas()
        {
            // act
            var result = _parser.Parse(DATA);
            
            // assert
            Assert.AreEqual(729.337m, result.GasMeterPosition);
        }
    }
}