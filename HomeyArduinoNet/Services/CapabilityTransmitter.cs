using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using HomeyArduinoNet.Configuration;
using HomeyArduinoNet.Dto;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace HomeyArduinoNet.Services
{
    public class CapabilityTransmitter: IDisposable
    {
        private bool _disposed;
        private readonly List<IDisposable> _subscriptions = new List<IDisposable>();
        private readonly HomeyDevice _device;

        public CapabilityTransmitter(HomeyDevice device)
        {
            _device = device;
            foreach (var capability in device.Api.OfType<HomeyApiCapability>())
            {
                _subscriptions.Add(capability.ValueChange.Subscribe(v => OnValueChange(v, capability).ConfigureAwait(false)));
            }
        }

        private async Task OnValueChange(HomeyEmitBody value, HomeyApiCapability capability)
        {
            using (var http = new HttpClient())
            {
                var url =
                    $"http://{_device.Master.Host}:{_device.Master.Port}/emit/{capability.Type}/{capability.Name}";
                DefaultContractResolver contractResolver = new DefaultContractResolver
                {
                    NamingStrategy = new CamelCaseNamingStrategy()
                };
                var json = JsonConvert.SerializeObject(value, new JsonSerializerSettings
                {
                    ContractResolver = contractResolver,
                    Formatting = Formatting.Indented
                });
                var content = new StringContent(json, Encoding.UTF8, "application/json");
                var result = await http.PostAsync(url, content);
            }
        }

        public void Dispose()
        {
            if (_disposed)
                return;

            _disposed = true;
            
            _subscriptions.ForEach(s => s.Dispose());
            _subscriptions.Clear();
        }
    }
}