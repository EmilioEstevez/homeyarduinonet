﻿using System.Threading.Tasks;
using HomeyArduinoNet.Configuration;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;

namespace HomeyArduinoNet
{
    public class HomeyDeviceHost
    {
        private readonly HomeyDevice _device;

        public HomeyDeviceHost(HomeyDevice device)
        {
            _device = device;
            Task.Run(() => CreateWebHostBuilder().Build().Run());
        }
        
        
        

        public IWebHostBuilder CreateWebHostBuilder() =>
            WebHost.CreateDefaultBuilder()
                .UseUrls(@"http://*:46639")
                .ConfigureServices(collection => { collection.AddSingleton(_device); })
                .UseStartup<Startup>();
    }
}