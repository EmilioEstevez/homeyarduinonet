using System;
using System.Threading.Tasks;
using HomeyArduinoNet.Dto;

namespace HomeyArduinoNet.Configuration
{
    public class HomeyApiAction : HomeyApi
    {
        private readonly Action _action;

        public HomeyApiAction(string name, Action action)
            : base(name, HomeyConstants.TYPE_ACTION)
        {
            _action = action;
        }

        public Task Run()
        {
            return Task.Run(() => _action());
        }
    }
}