using System;
using System.Net.Http;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using System.Threading.Tasks;
using HomeyArduinoNet.Dto;
using Newtonsoft.Json;

namespace HomeyArduinoNet.Configuration
{
    public class HomeyApiCapability : HomeyApi, IDisposable
    {
        private bool _disposed;
        private readonly IDisposable _valueChangeSubscription;
        private readonly ICapabilityStatus _capabilityStatus;
        private readonly Subject<HomeyEmitBody> _valueChange = new Subject<HomeyEmitBody>();

        public HomeyApiCapability(string name, ICapabilityStatus capabilityStatus)
            : base(name, HomeyConstants.TYPE_CAPABILITY)
        {
            _capabilityStatus = capabilityStatus;
            _valueChangeSubscription = capabilityStatus.ValueChange.Subscribe(v => OnValueChange(v));
        }

        [JsonIgnore]
        public IObservable<HomeyEmitBody> ValueChange => _valueChange.AsObservable();
        
        [JsonIgnore]
        public HomeyFieldType FieldType => _capabilityStatus.Type;
        
        private void OnValueChange(object value)
        {
            if (_disposed)
                return;
            
            _valueChange.OnNext(new HomeyEmitBody(_capabilityStatus.Type, value));
        }

        public HomeyResponse GetValue()
        {
            return new HomeyResponse(_capabilityStatus.Type, _capabilityStatus.Value);
        }

        public void Dispose()
        {
            if (_disposed)
                return;

            _disposed = true;
            _valueChangeSubscription.Dispose();
            _valueChange.Dispose();
        }
    }
}