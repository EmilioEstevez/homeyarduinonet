using System.Collections.Generic;

namespace HomeyArduinoNet.Configuration
{
    public class HomeyDevice
    {
        public string Id { get; set; }
        public string Version { get; set; }
        public string @Class { get; set; }
        public string Type { get; set; }
        public MasterConfiguration Master { get; set; }
        public List<HomeyApi> Api { get; set; }
    }
}