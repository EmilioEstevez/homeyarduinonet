namespace HomeyArduinoNet.Configuration
{
    public abstract class HomeyApi
    {
        public HomeyApi(string name, string type)
        {
            Name = name;
            Type = type;
        }
        
        public string Name { get; }
        public string Type { get; }
    }
}