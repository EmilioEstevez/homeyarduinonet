using System;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using HomeyArduinoNet.Dto;

namespace HomeyArduinoNet.Configuration
{
    public class CapabilityStatus: ICapabilityStatus
    {
        private object _value;
        private readonly Subject<object> _valueChange = new Subject<object>();

        public CapabilityStatus(HomeyFieldType type, object initialValue)
        {
            Type = type;
            _value = initialValue;
        }
        
        public HomeyFieldType Type { get; }
        public IObservable<object> ValueChange => _valueChange.AsObservable();

        public object Value
        {
            get => _value;
            set
            {
                if (_value == value)
                    return;

                _value = value;
                _valueChange.OnNext(_value);
            }
        }
    }
}