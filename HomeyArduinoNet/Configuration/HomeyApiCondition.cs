using HomeyArduinoNet.Dto;

namespace HomeyArduinoNet.Configuration
{
    public class HomeyApiCondition : HomeyApi
    {
        public HomeyApiCondition(string name)
            : base(name, HomeyConstants.TYPE_CONDITION)
        { }
    }
}