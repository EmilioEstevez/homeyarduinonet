namespace HomeyArduinoNet.Configuration
{
    public class MasterConfiguration
    {
        public string Host { get; set; } = "0.0.0.0";
        public int Port { get; set; } = 9999;
    }
}