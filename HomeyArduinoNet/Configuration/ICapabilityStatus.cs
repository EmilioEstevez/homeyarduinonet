using System;
using HomeyArduinoNet.Dto;

namespace HomeyArduinoNet.Configuration
{
    public interface ICapabilityStatus
    {
        HomeyFieldType Type { get; }
        IObservable<object> ValueChange { get; }
        object Value { get; }
    }
}