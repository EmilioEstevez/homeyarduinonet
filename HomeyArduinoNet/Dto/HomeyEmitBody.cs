using System;

namespace HomeyArduinoNet.Dto
{
    public class HomeyEmitBody
    {
        public HomeyEmitBody(HomeyFieldType fieldType, object argument)
        {
            Type = fieldType;
            Argument = argument;
        }
        
        public HomeyFieldType Type { get; }
        public object Argument { get; }
    }
}