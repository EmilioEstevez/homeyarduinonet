namespace HomeyArduinoNet.Dto
{
    public class HomeyConstants
    {
        public const string TYPE_ACTION 	= "act";
        public const string TYPE_CONDITION	= "con";
        public const string TYPE_CAPABILITY	= "cap";
        
        public const string CTYPE_STRING = "String";
        public const string CTYPE_INT = "Number";
        public const string CTYPE_FLOAT = "Number";
        public const string CTYPE_DOUBLE = "Number";
        public const string CTYPE_BOOL = "Boolean";
    }
}