namespace HomeyArduinoNet.Dto
{
    public enum HomeyFieldType
    {
        String,
        Number,
        Boolean
    }
}