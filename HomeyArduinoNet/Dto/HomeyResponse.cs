using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace HomeyArduinoNet.Dto
{
    public class HomeyResponse
    {
        public HomeyResponse(HomeyFieldType fieldType, object response)
        {
            Type = fieldType;
            Response = response;
        }
        
        [JsonProperty("t")]
        [JsonConverter(typeof(StringEnumConverter))]
        public HomeyFieldType Type { get; }

        [JsonProperty("r")]
        public object Response { get; }
    }
}