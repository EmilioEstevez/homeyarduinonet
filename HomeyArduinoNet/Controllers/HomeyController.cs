﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using HomeyArduinoNet.Configuration;
using HomeyArduinoNet.Dto;
using HomeyArduinoNet.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.CodeAnalysis;

namespace HomeyArduinoNet.Controllers
{
    [Route("")]
    [ApiController]
    public class HomeyController : ControllerBase
    {
        private readonly HomeyDevice _device;
        
        public HomeyController(HomeyDevice device)
        {
            _device = device;
        }
        
        // GET api/values
        [HttpGet]
        public ActionResult<HomeyDevice> Get()
        {
            return _device;
        }

        [HttpPost("sys/setmaster")]
        public async Task<ActionResult<bool>> SetMaster()
        {
            var address = "";
            using (StreamReader reader = new StreamReader(Request.Body, Encoding.UTF8))
                address = await reader.ReadToEndAsync();

            var hostIp = address.Split(':');
            _device.Master.Host = hostIp[0];
            _device.Master.Port = int.Parse(hostIp[1]);
            
            return true;
        }

        [HttpPost("act/{apiAction}")]
        public async Task<ActionResult<bool>> RunAction(string apiAction)
        {
            var data = "";
            using (StreamReader reader = new StreamReader(Request.Body, Encoding.UTF8))
                data = await reader.ReadToEndAsync();
            
            var action = _device.Api.OfType<HomeyApiAction>().FirstOrDefault(api => api.Name == apiAction);

            if (action == null) 
                return NotFound();
            
            await action.Run();
            return Ok();

        }

        
        public async Task<ActionResult<object>> ValidateCondition(string condition)
        {
            return new object();
        }

        [HttpGet("cap/{apiCapability}")]
        [HttpPost("cap/{apiCapability}")]
        public async Task<ActionResult<HomeyResponse>> GetCapability(string apiCapability)
        {
            var data = "";
            using (StreamReader reader = new StreamReader(Request.Body, Encoding.UTF8))
                data = await reader.ReadToEndAsync();
            
            var capability = _device.Api.OfType<HomeyApiCapability>().FirstOrDefault(api => api.Name == apiCapability);

            if (capability != null)
                return capability.GetValue();

            return NotFound();
        }
    }
}